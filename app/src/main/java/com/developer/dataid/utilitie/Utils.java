package com.developer.dataid.utilitie;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;

import com.developer.dataid.R;
import com.developer.dataid.permisos.PermisoListener;
import com.developer.dataid.permisos.ValidarPermiso;

/**
 * Created by dessis-aux42 on 23/08/17.
 */

public class Utils {
  public void verificarPermiso(final String permiso, final String msjPermisoBloqueado, final Context mContext){

    ValidarPermiso permisoChk = new ValidarPermiso((Activity) mContext, permiso);

    permisoChk.setOnPermisoListener(
        new PermisoListener() {
          @Override
          public void Permitido() {}

          @Override
          public void Denegado() {
            ActivityCompat.requestPermissions((Activity) mContext, new String[] {permiso}, 1000);
          }

          @Override
          public void Bloqueado() {
            showPermissionDialog(
                (Activity) mContext,"advertencia", msjPermisoBloqueado);
          }

          @Override
          public void NuncaPreguntado() {
            ActivityCompat.requestPermissions((Activity) mContext, new String[] {permiso}, 1000);
          }
        });
    permisoChk.checar();
  }
  private void showPermissionDialog(final Activity activity, String titulo, String mensaje) {

    showAlertDialog(
        titulo,
        mensaje,
        "Dar permiso",
        "Cancelar",
        new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            Uri uri = Uri.fromParts("package", activity.getPackageName(), "");
            mostrarConfiguraciones(activity, Settings.ACTION_APPLICATION_DETAILS_SETTINGS, uri);
          }
        }, activity);
  }
  public void showAlertDialog(
      String tituloDialogo,
      String mensaje,
      String positiveBtnTitle,
      String negativeBtnTitle,
      DialogInterface.OnClickListener onClickListener,
      Activity activity) {

    android.app.AlertDialog.Builder dialog =
        new android.app.AlertDialog.Builder(activity.getApplicationContext(), R.style.DateDialog);
    dialog.setTitle(tituloDialogo);
    dialog.setMessage(mensaje);
    dialog.setPositiveButton(positiveBtnTitle, onClickListener);
    if (!negativeBtnTitle.isEmpty()) dialog.setNegativeButton(negativeBtnTitle, null);
    dialog.show();
  }
  public void mostrarConfiguraciones(Activity activity, String configuracion, Uri uri) {
    activity.startActivityForResult(new Intent(configuracion).setData(uri), 0);
  }
}
