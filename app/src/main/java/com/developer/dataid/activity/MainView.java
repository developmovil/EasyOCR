package com.developer.dataid.activity;

/**
 * Created by dessis-aux42 on 23/08/17.
 */

public interface MainView {

  void mostrarInformacionOCR(String respuesta);
  void mostrarProgress();
  void ocultarProgress();

}
