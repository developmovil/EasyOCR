package com.developer.dataid.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.developer.dataid.R;
import com.developer.dataid.presenter.MainPresenter;
import com.developer.dataid.utilitie.Utils;
import com.mindorks.paracamera.Camera;

import java.io.ByteArrayOutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MainActivity extends AppCompatActivity implements MainView{
  private String TAG = getClass().getSimpleName();
  private Camera mCamera;
  private ProgressDialog mProgressDialog;
  @BindView(R.id.txvRespuesta)
  TextView txvRespuesta;
  @BindView(R.id.imgView)
  ImageView imageView;
  MainPresenter mainPresenter;
  Utils mUtils;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    ButterKnife.bind(this);

    this.mUtils = new Utils();
    this.mainPresenter = new MainPresenter(getApplicationContext(), this);
  }

  @OnClick(R.id.fabCamera)
  public void tomarFoto(){
    if (ActivityCompat.checkSelfPermission(
        this, Manifest.permission.READ_EXTERNAL_STORAGE)
        == PackageManager.PERMISSION_GRANTED){
      mCamera =
          new Camera.Builder()
              .resetToCorrectOrientation(true)
              .setTakePhotoRequestCode(400)
              .setDirectory("pics")
              .setName("afiliacion_" + System.currentTimeMillis())
              .setImageFormat(Camera.IMAGE_PNG)
              .setCompression(1)
              .setImageHeight(200)
              .build(this);
      try {
        mCamera.takePicture();
      } catch (Exception e) {
        Log.e(TAG + ":activarCamara", e.toString());
      }
    }else{
      try {
        mUtils.verificarPermiso(
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            this
                .getResources()
                .getString(R.string.str_mensajePermisoLecturaEscritura)
            ,this);
      } catch (Exception e) {
        Log.e(TAG + ":onClickFragment", e.toString());
      }
    }
  }

  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == Camera.REQUEST_TAKE_PHOTO) {
      Bitmap bitmap = mCamera.getCameraBitmap();
      if (bitmap != null) {
        String encodeImage = encodeImage(mCamera.getCameraBitmap());
        mainPresenter.llamarOCR(encodeImage);
      }
    }
  }

  public String encodeImage(Bitmap bitmap) {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    bitmap.compress(Bitmap.CompressFormat.PNG, 50, baos);
    byte[] bytes = baos.toByteArray();
    String imgComp = Base64.encodeToString(bytes, Base64.DEFAULT);
    return imgComp;
  }

  @Override
  public void mostrarInformacionOCR(String respuesta) {
    txvRespuesta.setText(respuesta);
  }

  @Override
  public void mostrarProgress() {
    mProgressDialog = new ProgressDialog(this, R.style.AppTheme_light_Dialog);
    mProgressDialog.setIndeterminate(true);
    mProgressDialog.setMessage("Consultando OCR...");
    mProgressDialog.setCanceledOnTouchOutside(false);
    mProgressDialog.setCancelable(false);
    mProgressDialog.show();
  }

  @Override
  public void ocultarProgress() {
    mProgressDialog.dismiss();
  }
}
