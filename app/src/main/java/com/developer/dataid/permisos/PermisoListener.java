package com.developer.dataid.permisos;

public interface PermisoListener {
  void NuncaPreguntado();

  void Permitido();

  void Denegado();

  void Bloqueado();
}
