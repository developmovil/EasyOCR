package com.developer.dataid.presenter;

import android.content.Context;

import com.developer.dataid.activity.MainView;
import com.developer.dataid.interactor.MainInteractor;

/**
 * Created by dessis-aux42 on 23/08/17.
 */

public class MainPresenter implements IMainPresenter,
IMainPresenter.OnConsultaOCRFinishedListener{

  private Context mContext;
  private MainView mMainView;
  private MainInteractor mainInteractor;

  public MainPresenter(Context context, MainView mainView){
    this.mContext = context;
    this.mMainView = mainView;
    this.mainInteractor = new MainInteractor(mContext);
  }

  @Override
  public void llamarOCR(String base64) {
    mMainView.mostrarProgress();
    mainInteractor.consultarOCR(this,base64);
  }

  @Override
  public void onSuccess(String respuesta) {
    mMainView.ocultarProgress();
    mMainView.mostrarInformacionOCR(respuesta);
  }

  @Override
  public void onFailed(String error) {
    mMainView.ocultarProgress();
    mMainView.mostrarInformacionOCR(error);
  }
}
